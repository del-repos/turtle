# turtle

Companion repo for our demo

## :chart_with_upwards_trend: Presentation
- [(Demo) Programming Art](https://docs.google.com/presentation/d/1TrvyQW6K9J66GtMmx2puQBh2jq4Pb4cmVZGENhn2qBo)

## :dart: Goal
- [x] Show the basics of `turtle`
- [x] Show the basics of `python3`
- [x] Show some art with `python3` and `turtle`

## :microscope: Technologies
- language: `python3`
- libraries: `turtle`
