import turtle as t

def draw_shape(length, sides):
    angle = 360 / sides

    for _ in range(sides):
        t.forward(length)
        t.left(angle)


length = 30

t.back(length)
t.forward(length)
for _ in range(7):
    draw_shape(length, 3)
    t.forward(length)
t.forward(length)
t.back(length)
t.left(120)
t.forward(length)
t.right(120)
t.forward(length)
t.back(length*8)

# draw_shape(length,3)
# t.forward(length)
# draw_shape(length,3)
# t.forward(length)

t.done()