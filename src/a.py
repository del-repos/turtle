import turtle as t

# Single -line comment
"""
Multi-line Comment
Comments
This won't run!
"""
# Variables - group values for reusability

length = 100
angle = 90

t.forward(length)
t.right(angle)
t.forward(length)
t.right(angle)
t.forward(length)
t.right(angle)
t.forward(length)
t.right(angle)

# Loops - get computer to repeat commands instead of writing it out

length = 100
base_angle = 360
loops = 6

for _ in range(loops):
    t.forward(length)
    t.right(base_angle/loops)


# Conditional - do some instruction if some condition is met

length = 100
angle = 90

if angle == 90:
    for _ in range(4):
        t.forward(length)
        t.right(angle)

# Functions - group some instructions for reusability

def draw_square(length):
    angle = 90
    for _ in range(4):
        t.forward(length)
        t.right(angle)


t.done()

